# !/usr/bin/python

"""
Task 2:Implementation of the experiment described in the paper with title
"""

import os
import time
import matplotlib.pyplot as plt
from mininet.net import Mininet
from mininet.node import Controller, OVSKernelSwitch, OVSKernelAP
from mininet.link import TCLink
from mininet.log import setLogLevel, debug
from mininet.cli import CLI

import sys
gnet=None


# Data Files
c0throughput1 = 'c0throughput1.data'
clientthroughput1 = 'clientthroughput1.data'
c0throughput2 = 'c0throughput2.data'
clientthroughput2 = 'clientthroughput2.data'
c0throughput3 = 'c0throughput3.data'
clientthroughput3 = 'clientthroughput3.data'

c0latency1 = 'c0latency1.data'
c0latency2 = 'c0latency2.data'
c0latency3 = 'c0latency3.data'

c0client1 = 'c0client1.data'
c0client2 = 'c0client2.data'
c0client3 = 'c0client3.data'



# Implement the graphic function in order to demonstrate the network measurements
# Hint: You can save the measurement in an output file and then import it here
def graphic():
    plt.clf()

def apply_experiment(car,client,switch):

    Time = 20

    ################################################################################ 
    #   1) Add the flow rules below and the necessary routing commands
    #
    #   Hint 1: For the OpenFlow rules you can either delete and add rules
    #           or modify rules (using mod-flows command)       
    #   Example: os.system('ovs-ofctl mod-flows switch in_port=1,actions=output:2')
    #
    #   Hint 2: For the routing commands check the configuration 
    #           at the beginning of the experiment.
    #
    #   2) Calculate Network Measurements using IPerf or command line tools(ifconfig)
    #       Hint: Remember that you can insert commands via the mininet
    #       Example: car[0].cmd('ifconfig bond0 | grep \"TX packets\" >> %s' % output.data)
    #
    #               ***************** Insert code below *********************  
    #################################################################################

    #time.sleep(2)
    print "Applying first phase"

    os.system('ovs-ofctl mod-flows switch in_port=1,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=3,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:1,3')
    os.system('ovs-ofctl mod-flows switch in_port=2,actions=drop')
    
    car.cmd('ip route add 200.0.10.2 via 200.0.10.100')

    car.cmd('ping 200.0.10.2  -c 20 >> %s &' % c0latency1)

    client.cmd('iperf -s -u -i 1 >> %s &' % c0client1)
    car.cmd('iperf -c 200.0.10.2 -u -i 1 -t 20')

    timeout = time.time() + Time
    currentTime = time.time()
    i = 0
    while True:
        if time.time() > timeout:
            break;
        if time.time() - currentTime >= i:
            car.cmd('ifconfig bond0 | grep \"bytes\" >> %s' % c0throughput1)
            client.cmd('ifconfig client-eth0 | grep \"bytes\" >> %s' % clientthroughput1)
            i += 0.5
    

    print "Moving nodes"
    car.moveNodeTo('150,100,0')

    #time.sleep(2)
    print "Applying second phase"

    ################################################################################ 
    #   1) Add the flow rules below and the necessary routing commands
    #
    #   Hint 1: For the OpenFlow rules you can either delete and add rules
    #           or modify rules (using mod-flows command)       
    #   Example: os.system('ovs-ofctl mod-flows switch in_port=1,actions=output:2')
    #
    #   Hint 2: For the routing commands check the configuration 
    #           at the beginning of the experiment.
    #
    #   2) Calculate Network Measurements using IPerf or command line tools(ifconfig)
    #       Hint: Remember that you can insert commands via the mininet
    #       Example: car[0].cmd('ifconfig bond0 | grep \"TX packets\" >> %s' % output.data)
    #
    #               ***************** Insert code below *********************  
    #################################################################################

    os.system('ovs-ofctl mod-flows switch in_port=1,actions=drop')
    os.system('ovs-ofctl mod-flows switch in_port=2,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:2,3')
    os.system('ovs-ofctl mod-flows switch in_port=3,actions=output:4')

    car.cmd('ping 200.0.10.2  -c 20 >> %s &' % c0latency2)

    client.cmd('iperf -s -u -i 1 >> %s &' % c0client2)
    car.cmd('iperf -c 200.0.10.2 -u -i 1 -t 20')

    timeout = time.time() + Time
    currentTime = time.time()
    i = 0
    while True:
        if time.time() > timeout:
            break;
        if time.time() - currentTime >= i:
            car.cmd('ifconfig bond0 | grep \"bytes\" >> %s' % c0throughput2)
            client.cmd('ifconfig client-eth0 | grep \"bytes\" >> %s' % clientthroughput2)
            i += 0.5

    print "Moving nodes"
    car.moveNodeTo('190,100,0')

    #time.sleep(2)
    print "Applying third phase"

    ################################################################################ 
    #   1) Add the flow rules below and the necessary routing commands
    #
    #   Hint 1: For the OpenFlow rules you can either delete and add rules
    #           or modify rules (using mod-flows command)       
    #   Example: os.system('ovs-ofctl mod-flows switch in_port=1,actions=output:2')
    #
    #   Hint 2: For the routing commands check the configuration 
    #           at the beginning of the experiment.
    #
    #   2) Calculate Network Measurements using IPerf or command line tools(ifconfig)
    #       Hint: Remember that you can insert commands via the mininet
    #       Example: car[0].cmd('ifconfig bond0 | grep \"TX packets\" >> %s' % output.data)
    #
    #               ***************** Insert code below *********************  
    #################################################################################

    os.system('ovs-ofctl mod-flows switch in_port=1,actions=drop')
    os.system('ovs-ofctl mod-flows switch in_port=3,actions=drop')
    os.system('ovs-ofctl mod-flows switch in_port=2,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:2')

    car.cmd('ping 200.0.10.2  -c 20 >> %s &' % c0latency3)

    client.cmd('iperf -s -u -i 1 >> %s &' % c0client3)
    car.cmd('iperf -c 200.0.10.2 -u -i 1 -t 20')

    timeout = time.time() + Time
    currentTime = time.time()
    i = 0
    while True:
        if time.time() > timeout:
            break;
        if time.time() - currentTime >= i:
            car.cmd('ifconfig bond0 | grep \"bytes\" >> %s' % c0throughput3)
            client.cmd('ifconfig client-eth0 | grep \"bytes\" >> %s' % clientthroughput3)
            i += 0.5

def topology():
    "Create a network."
    net = Mininet(controller=Controller, link=TCLink, switch=OVSKernelSwitch, accessPoint=OVSKernelAP)
    global gnet
    gnet = net

    print "*** Creating nodes"
    mycar = net.addCar('car%s' % (0), wlans=2, ip='10.0.0.%s/8' % (0 + 1), \
    mac='00:00:00:00:00:0%s' % 0, mode='b')


    eNodeB1 = net.addAccessPoint('eNodeB1', ssid='eNodeB1', dpid='1000000000000000', mode='ac', channel='1', position='80,75,0', range=60)
    eNodeB2 = net.addAccessPoint('eNodeB2', ssid='eNodeB2', dpid='2000000000000000', mode='ac', channel='6', position='180,75,0', range=70)
    rsu1 = net.addAccessPoint('rsu1', ssid='rsu1', dpid='3000000000000000', mode='g', channel='11', position='140,120,0', range=50)
    c1 = net.addController('c1', controller=Controller)
    client = net.addHost ('client')
    switch = net.addSwitch ('switch', dpid='4000000000000000')

    net.plotNode(client, position='125,230,0')
    net.plotNode(switch, position='125,200,0')

    print "*** Configuring wifi nodes"
    net.configureWifiNodes()

    print "*** Creating links"
    net.addLink(eNodeB1, switch)
    net.addLink(eNodeB2, switch)
    net.addLink(rsu1, switch)
    net.addLink(switch, client)

    print "*** Starting network"
    net.build()
    c1.start()
    eNodeB1.start([c1])
    eNodeB2.start([c1])
    rsu1.start([c1])
    switch.start([c1])

    for sw in net.vehicles:
        sw.start([c1])

    client.cmd('ifconfig client-eth0 200.0.10.2')
    mycar.cmd('modprobe bonding mode=3')
    mycar.cmd('ip link add bond0 type bond')
    mycar.cmd('ip link set bond0 address 02:01:02:03:04:08')
    mycar.cmd('ip link set mycar-eth0 down')
    mycar.cmd('ip link set mycar-eth0 address 00:00:00:00:00:11')
    mycar.cmd('ip link set mycar-eth0 master bond0')
    mycar.cmd('ip link set mycar-wlan0 down')
    mycar.cmd('ip link set mycar-wlan0 address 00:00:00:00:00:15')
    mycar.cmd('ip link set mycar-wlan0 master bond0')
    mycar.cmd('ip link set mycar-wlan1 down')
    mycar.cmd('ip link set mycar-wlan1 address 00:00:00:00:00:13')
    mycar.cmd('ip link set mycar-wlan1 master bond0')
    mycar.cmd('ip addr add 200.0.10.100/24 dev bond0')
    mycar.cmd('ip link set bond0 up')

    """plot graph"""
    net.plotGraph(max_x=250, max_y=250)

    net.startGraph()

    # Uncomment and modify the two commands below to stream video using VLC
    mycar.cmdPrint("sudo vlc -vvv bunnyMob.mp4 --sout '#duplicate{dst=rtp{dst=200.0.10.2,port=5004,mux=ts},dst=display}' :sout-keep &")
    client.cmdPrint("sudo vlc rtp://@200.0.10.2:5004 &")

    mycar.moveNodeTo('95,100,0')

    os.system('ovs-ofctl del-flows switch')

    time.sleep(3)

    apply_experiment(mycar,client,switch)

    # Uncomment the line below to generate the graph that you implemented
    graphic()

    # kills all the xterms that have been opened
    os.system('pkill xterm')

    print "*** Running CLI"
    CLI(net)

    print "*** Stopping network"
    net.stop()

if __name__ == '__main__':
    setLogLevel('info')
    try:
        topology()
    except:
        type = sys.exc_info()[0]
        error = sys.exc_info()[1]
        traceback = sys.exc_info()[2]
        print ("Type: %s" % type)
        print ("Error: %s" % error)
        print ("Traceback: %s" % traceback)
        if gnet != None:
            gnet.stop()
        else:
            print "No network was created..."
