# !/usr/bin/python

"""
Task 1: Implementation of the experiment described in the paper with title: 
"From Theory to Experimental Evaluation: Resource Management in Software-Defined Vehicular Networks"
http://ieeexplore.ieee.org/document/7859348/ 
"""

import os
import time
import matplotlib.pyplot as plt
from mininet.net import Mininet
from mininet.node import Controller, OVSKernelSwitch, OVSKernelAP
from mininet.link import TCLink
from mininet.log import setLogLevel, debug
from mininet.cli import CLI

server_throughput = 'server_throughput.data'
client_throughput = 'client_throughput.data'
received_data = 'received_d.data'
transmited_data = 'transmited_d.data'

latency_data='latency.data'

client_packet_drop_data= 'client_packet_drop.data'
server_packet_drop_data = 'server_packet_drop.data'
packet_loss_data = 'packet_loss.data'

client_error_data = 'client_error.data'

temp_data = 'temp.data'

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#c03_latency_data = 'c03_latency.data'
#c3cl_latency_data = 'c3cl_latency.data'

def add_latency():
    f1 = open('./' + c03_latency_data, 'r')
    car0 = f1.readlines()
    f1.close()

    f2 = open('./' + c3cl_latency_data, 'r')
    car3 = f2.readlines()
    f2.close()

    f3 = open('./' + latency_data, 'w')

    j = 0
    for x in car0:
        res = float(x) + float(car3[j])
        print >> f3, res
        j += 1

    f3.close()

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

import sys
gnet=None
taskTime = 20

def graphic_1():     #synarthsh pou sxediazei to diagramma rythmo metavolhs throughput & arithmou paketwn (Server,Client)
    f1 = open('./' + server_throughput, 'r')
    s_throughput = f1.readlines()
    f1.close()

    f2 = open('./' + client_throughput, 'r')
    c_throughput = f2.readlines()
    f2.close()

    f3 = open('./' + transmited_data, 'r')
    t_data = f3.readlines()
    f3.close()

    f4 = open('./' + received_data, 'r')
    r_data = f4.readlines()
    f4.close()

    measured_time = []

    t1 = []
    t2 = []
    d1 = []
    d2 = []

    tt1 = []
    tt2 = []
    dd1 = []
    dd2 = []

    j = 0
    for x in s_throughput:    
        t1.append(int(x))
        if len(t1) > 1:
            tt1.append(t1[j] - t1[j-1])
        j += 1

    j = 0
    for x in c_throughput:    
        t2.append(int(x))
        if len(t2) > 1:
            tt2.append(t2[j] - t2[j - 1])
        j += 1

    j = 0
    for x in t_data:
        d1.append(int(x))
        if len(d1) > 1:
            dd1.append(d1[j] - d1[j - 1])
        j += 1

    j = 0
    for x  in r_data:
        d2.append(int(x))
        if len(d2) > 1:
            dd2.append(d2[j] - d2[j - 1])
        j += 1

    j = 0
    for x in range(len(dd1)):    
        measured_time.append(j)
        j += 1
    
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.plot(measured_time, tt2, color = 'red', label = 'Throughput (Client)', markevery = 7, linewidth = 1)
    ax1.plot(measured_time, tt1, color = 'black', label = 'Throughput (Server)', markevery = 7, linewidth = 1)   

    ax2 = ax1.twinx()
    ax2.plot(measured_time, dd2, color = 'red', label = 'Received Data (Client)', ls = '-.', markevery = 7, linewidth = 1)
    ax2.plot(measured_time, dd1, color = 'black', label = 'Transmited Data (Server)', ls = '-.', markevery = 7, linewidth = 1)

    ax1.legend(loc=1, borderaxespad=0., fontsize=12)
    ax2.legend(loc=2, borderaxespad=0., fontsize=12)
    ax1.set_yscale('log')
    ax1.set_ylabel("Throughput (bytes/sec)", fontsize=18)
    ax1.set_xlabel("Time (seconds)", fontsize = 18)
    ax2.set_ylabel("Number of Packets", fontsize=18)  

    plt.show()
    plt.savefig("graphic.jpg")


def graphic_2():     #synarthsh pou sxediazei to diagramma pou aniparista to latency synarthsei tou xronou

    f = open('./' + latency_data, 'r')
    l_data = f.readlines()
    f.close()
 
    measured_time = []
    d = []

    for x in l_data:  
        d.append(x)

    j = 0
    for x in range(len(d)): 
        measured_time.append(j)
        j += 1
        
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(measured_time, d, color = 'red', markevery = 7, linewidth = 1)
    ax.legend(loc=1, borderaxespad=0., fontsize=12)
    ax.set_ylabel("Latency (ms)", fontsize=18)
    ax.set_xlabel("Time (seconds)", fontsize = 18)
    
    plt.show()
    plt.savefig("latency_graphic.jpg")


def graphic_3():                  #syrthsh pou sxediazei ton ry8mo twn dropped paketwn (Server,Client)

    f1 = open('./' + client_packet_drop_data, 'r')
    cl_pcdrop_data = f1.readlines()
    f1.close()    

    f2 = open('./' + server_packet_drop_data, 'r')
    sr_pcdrop_data = f2.readlines()
    f2.close()    

    measured_time = []    
    d1 = []
    d2 = []
    dd1 = []
    dd2 = []

    j= 0
    for x in cl_pcdrop_data:
        d1.append(int(x))
        if len(d1) > 1:
            dd1.append(d1[j] - d1[j-1])
        j += 1

    j = 0
    for x in sr_pcdrop_data:
        d2.append(int(x))
        if len(d2) > 1:
            dd2.append(d2[j] - d2[j-1])
        j += 1

    j = 0
    for x in range(len(dd1)):    
        measured_time.append(j)
        j += 1

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(measured_time, dd1, color = 'red', label = 'Packet Drop (Client)', markevery = 7, linewidth = 1)
    ax.plot(measured_time, dd2, color = 'black', label = 'Packet Drop (Server)', markevery = 7, linewidth = 1)   

    ax.legend(loc=1, borderaxespad=0., fontsize=12)
    ax.set_ylabel("Packet Drop", fontsize=18)
    ax.set_xlabel("Time (seconds)", fontsize = 18)
    
    plt.show()
    plt.savefig("packet_drop_graphic.jpg")


def mo_latency_phase2():                 #synarthsh pu xrhsimopoieitai sthn deuterh fash kai gia ka8e ekpebomeno paketo ICMP tou ping
                                                                           #vrhskei to meso latency pou tou antistoixei dedomenou oti gia kathe ekpebomeno packeto epistrefontai 4 paketa
                                                                           #e3aitias oti yfistatai wireless bicasting se afth thn prwth fash tou peiramatos
    f = open('./' + temp_data, 'r')
    t_data = f.readlines()
    f.close()

    f2 = open('./' + latency_data, 'a')

    mo = 0.0
    sum = 0.0
    x = 0      
    while x < len(t_data) - 4:
        for z in range(4):
            sum += float(t_data[x+z])
        mo = sum/4
        print >> f2, round(float(mo), 2)
        sum = 0.0
        x += 4

    f2.close()       
        
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def apply_experiment(car,client,switch):
    
    #time.sleep(2)
    print "Applying first phase"

#kanones OpenFlow sto Switch kai stis epimerous keraies pou entopizontai sthn topologia

    os.system('ovs-ofctl mod-flows switch in_port=1,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:1')
    os.system('ovs-ofctl mod-flows switch in_port=2,actions=drop')
    os.system('ovs-ofctl mod-flows switch in_port=3,actions=drop')
    
    os.system('ovs-ofctl del-flows eNodeB1')
    os.system('ovs-ofctl del-flows eNodeB2')
    os.system('ovs-ofctl del-flows rsu1')

#kanones dromologhshs pou apaitountai

    car[0].cmd('ip route add 200.0.10.2 via 200.0.10.50')
    client.cmd('ip route add 200.0.10.100 via 200.0.10.150')
    car[3].cmd('echo 1 > /proc/sys/net/ipv4/ip_forward')

    car[0].cmd('ping -c 40 -i 1 200.0.10.2 | grep \"from\" | awk -F\'=\' \'{print $4}\' | awk -F\' \' \'{ print $1 }\' >> %s' % latency_data)

    #car[0].cmd('ping -c 40 -i 1 200.0.10.2 | grep \"packets transmitted\" | awk -F\',\' \'{print $3}\' >> %s' % packet_loss_data)

#******************************************************************************************************************************
#se periptwsh pou epi8ymoume na mazepsoume to latency ws to a8roisma twn latency meta3y car0 -> car3 & car3 ->  client
#proairetika exei dhmiourgh8ei kai h synarthsh h opoia epitelei thn sygkekrimenh leitourgia, h "def add_latency()"

    #car[0].cmd('ping -c 40 -i 1 10.0.0.4 | grep \"from\" | awk -F\'=\' \'{print $4}\' | awk -F\' \' \'{ print $1 }\' >> %s' % c03_latency_data)
    #car[3].cmd('ping -c 40 -i 1 200.0.10.2 | grep \"from\" | awk -F\'=\' \'{print $4}\' | awk -F\' \' \'{ print $1 }\' >> %s' % c3cl_latency_data)
    #add_latency()

#******************************************************************************************************************************

    endTime = time.time() + taskTime
    currentTime = time.time()

    j = 0
    while True:
        if time.time() > endTime:
            break;
        if time.time() - currentTime >= j:
            car[0].cmd('ifconfig bond0 | grep \"TX packets\" | awk -F\':\' \'{ print $2 }\' | awk -F\' \' \'{ print $1 }\'    >> %s' % transmited_data)
            client.cmd('ifconfig client-eth0 | grep "RX packets"  | awk -F\':\' \'{ print $2}\' | awk -F\' \' \'{ print $1 }\' >> %s' % received_data)
            car[0].cmd('ifconfig bond0 | grep \"bytes\" | awk -F\':\' \'{ print $3 }\' | awk -F\' \' \'{ print $1 }\'   >> %s' % server_throughput)
            client.cmd('ifconfig client-eth0 | grep \"bytes\" | awk -F\':\' \'{ print $2 }\' | awk -F\' \' \'{ print $1 }\' >> %s' % client_throughput)

            #client.cmd('ifconfig client-eth0 |  grep \"RX packets\"  | awk -F\':\' \'{ print $4 }\' | awk -F\' \' \'{ print $1 }\' >> %s' % client_packet_drop_data)
            #car[0].cmd('ifconfig bond0 | grep \"TX packets\" | awk -F\':\' \'{ print $4 }\'  | awk -F\' \' \'{ print $1 }\'  >> %s' % server_packet_drop_data) 

            #client.cmd('ifconfig client-eth0 |  grep \"RX packets\"  | awk -F\':\' \'{ print $3 }\' | awk -F\' \' \'{ print $1 }\' >> %s' % client_error_data)
           
            j += 0.5

 
    CLI(gnet)

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    print "Moving nodes"
    car[0].moveNodeTo('150,100,0')
    car[1].moveNodeTo('120,100,0')
    car[2].moveNodeTo('90,100,0')
    car[3].moveNodeTo('70,100,0')

    #time.sleep(2)
    print "Applying second phase"

#kanones OpenFlow sto Switch kai stis epimerous keraies pou entopizontai sthn topologia

    os.system('ovs-ofctl mod-flows switch in_port=2,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=3,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:2,3')
    os.system('ovs-ofctl mod-flows switch in_port=1,actions=drop')

    os.system('ovs-ofctl del-flows eNodeB1')
    os.system('ovs-ofctl del-flows eNodeB2')
    os.system('ovs-ofctl del-flows rsu1')

#kanones dropologhshs pou aneroun ekeinous pou pros8e8hkan sthn phase 1, ka8ws se afto to shmeio den xreiazontai

    car[0].cmd('ip route del 200.0.10.2 via 200.0.10.50')
    client.cmd('ip route del 200.0.10.100 via 200.0.10.150')

    car[0].cmd('ping -c 40 -i 1 200.0.10.2 | grep \"from\" | awk -F\'=\' \'{print $4}\' | awk -F\' \' \'{ print $1 }\'  >> %s' % temp_data)
    mo_latency_phase2()

    #car[0].cmd('ping -c 40 -i 1 200.0.10.2  | grep \"packets transmitted\" | awk -F\',\' \'{print $4}\' >> %s' % packet_loss_data)

    endTime = time.time() + taskTime
    currentTime = time.time()

    j = 0
    while True:
        if time.time() > endTime:
            break;
        if time.time() - currentTime >= j:
            car[0].cmd('ifconfig bond0 | grep \"TX packets\" | awk -F\':\' \'{ print $2 }\' | awk -F\' \' \'{ print $1 }\'    >> %s' % transmited_data)
            client.cmd('ifconfig client-eth0 | grep "RX packets"  | awk -F\':\' \'{ print $2}\' | awk -F\' \' \'{ print $1 }\' >> %s' % received_data)
            car[0].cmd('ifconfig bond0 | grep \"bytes\" | awk -F\':\' \'{ print $3 }\' | awk -F\' \' \'{ print $1 }\'   >> %s' % server_throughput)
            client.cmd('ifconfig client-eth0 | grep \"bytes\" | awk -F\':\' \'{ print $2 }\' | awk -F\' \' \'{ print $1 }\' >> %s' % client_throughput)

            #client.cmd('ifconfig client-eth0 |  grep \"RX packets\"  | awk -F\':\' \'{ print $4 }\' | awk -F\' \' \'{ print $1 }\' >> %s' % client_packet_drop_data)
            #car[0].cmd('ifconfig bond0 | grep \"TX packets\" | awk -F\':\' \'{ print $4 }\'  | awk -F\' \' \'{ print $1 }\'  >> %s' % server_packet_drop_data) 

            #client.cmd('ifconfig client-eth0 |  grep \"RX packets\"  | awk -F\':\' \'{ print $3 }\' | awk -F\' \' \'{ print $1 }\' >> %s' % client_error_data)
            
            j += 0.5

    CLI(gnet)

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    print "Moving nodes"
    car[0].moveNodeTo('190,100,0')
    car[1].moveNodeTo('150,100,0')
    car[2].moveNodeTo('120,100,0')
    car[3].moveNodeTo('90,100,0')
    
    #time.sleep(2)
    print "Applying third phase"

#kanones OpenFlow sto Switch kai stis epimerous keraies pou entopizontai sthn topologia

    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:2')    
    os.system('ovs-ofctl mod-flows switch in_port=3,actions=drop')

    os.system('ovs-ofctl del-flows eNodeB1')
    os.system('ovs-ofctl del-flows eNodeB2')
    os.system('ovs-ofctl del-flows rsu1')

    car[0].cmd('ping -c 40 -i 1 200.0.10.2 | grep \"from\" | awk -F\'=\' \'{print $4}\' | awk -F\' \' \'{ print $1 }\' >> %s' % latency_data)

    #car[0].cmd('ping -c 40 -i 1 200.0.10.2 | grep \"packets transmitted\" | awk -F\',\' \'{print $3}\' >> %s' % packet_loss_data)

    endTime = time.time() + taskTime
    currentTime = time.time()

    j = 0
    while True:
        if time.time() > endTime:
            break;
        if time.time() - currentTime >= j:
            car[0].cmd('ifconfig bond0 | grep \"TX packets\" | awk -F\':\' \'{ print $2 }\' | awk -F\' \' \'{ print $1 }\'    >> %s' % transmited_data)
            client.cmd('ifconfig client-eth0 | grep "RX packets"  | awk -F\':\' \'{ print $2}\' | awk -F\' \' \'{ print $1 }\' >> %s' % received_data)
            car[0].cmd('ifconfig bond0 | grep \"bytes\" | awk -F\':\' \'{ print $3 }\' | awk -F\' \' \'{ print $1 }\'   >> %s' % server_throughput)
            client.cmd('ifconfig client-eth0 | grep \"bytes\" | awk -F\':\' \'{ print $2 }\' | awk -F\' \' \'{ print $1 }\' >> %s' % client_throughput)

            #client.cmd('ifconfig client-eth0 |  grep \"RX packets\"  | awk -F\':\' \'{ print $4 }\' | awk -F\' \' \'{ print $1 }\' >> %s' % client_packet_drop_data)
            #car[0].cmd('ifconfig bond0 | grep \"TX packets\" | awk -F\':\' \'{ print $4 }\'  | awk -F\' \' \'{ print $1 }\'  >> %s' % server_packet_drop_data) 

            #client.cmd('ifconfig client-eth0 |  grep \"RX packets\"  | awk -F\':\' \'{ print $3 }\' | awk -F\' \' \'{ print $1 }\' >> %s' % client_error_data)            
       
            j += 0.5

    CLI(gnet)

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
def topology():
    "Create a network."
    net = Mininet(controller=Controller, link=TCLink, switch=OVSKernelSwitch, accessPoint=OVSKernelAP)
    global gnet
    gnet = net

    print "*** Creating nodes"
    car = []
    stas = []
    for x in range(0, 4):
        car.append(x)
        stas.append(x)
    for x in range(0, 4):
        car[x] = net.addCar('car%s' % (x), wlans=2, ip='10.0.0.%s/8' % (x + 1), \
        mac='00:00:00:00:00:0%s' % x, mode='b')

    
    eNodeB1 = net.addAccessPoint('eNodeB1', ssid='eNodeB1', dpid='1000000000000000', mode='ac', channel='1', position='80,75,0', range=60)
    eNodeB2 = net.addAccessPoint('eNodeB2', ssid='eNodeB2', dpid='2000000000000000', mode='ac', channel='6', position='180,75,0', range=70)
    rsu1 = net.addAccessPoint('rsu1', ssid='rsu1', dpid='3000000000000000', mode='g', channel='11', position='140,120,0', range=40)
    c1 = net.addController('c1', controller=Controller)
    client = net.addHost ('client')
    switch = net.addSwitch ('switch', dpid='4000000000000000')

    net.plotNode(client, position='125,230,0')
    net.plotNode(switch, position='125,200,0')

    print "*** Configuring wifi nodes"
    net.configureWifiNodes()

    print "*** Creating links"
    net.addLink(eNodeB1, switch)
    net.addLink(eNodeB2, switch)
    net.addLink(rsu1, switch)
    net.addLink(switch, client)
    

    print "*** Starting network"
    net.build()
    c1.start()
    eNodeB1.start([c1])
    eNodeB2.start([c1])
    rsu1.start([c1])
    switch.start([c1])

    for sw in net.vehicles:
        sw.start([c1])

    i = 1
    j = 2
    for c in car:
        c.cmd('ifconfig %s-wlan0 192.168.0.%s/24 up' % (c, i))
        c.cmd('ifconfig %s-eth0 192.168.1.%s/24 up' % (c, i))
        c.cmd('ip route add 10.0.0.0/8 via 192.168.1.%s' % j)
        i += 2
        j += 2

    i = 1
    j = 2
    for v in net.vehiclesSTA:
        v.cmd('ifconfig %s-eth0 192.168.1.%s/24 up' % (v, j))
        v.cmd('ifconfig %s-mp0 10.0.0.%s/24 up' % (v, i))
        v.cmd('echo 1 > /proc/sys/net/ipv4/ip_forward')
        i += 1
        j += 2

    for v1 in net.vehiclesSTA:
        i = 1
        j = 1
        for v2 in net.vehiclesSTA:
            if v1 != v2:
                v1.cmd('route add -host 192.168.1.%s gw 10.0.0.%s' % (j, i))
            i += 1
            j += 2

    client.cmd('ifconfig client-eth0 200.0.10.2')
    net.vehiclesSTA[0].cmd('ifconfig car0STA-eth0 200.0.10.50')

    car[0].cmd('modprobe bonding mode=3')
    car[0].cmd('ip link add bond0 type bond')
    car[0].cmd('ip link set bond0 address 02:01:02:03:04:08')
    car[0].cmd('ip link set car0-eth0 down')
    car[0].cmd('ip link set car0-eth0 address 00:00:00:00:00:11')
    car[0].cmd('ip link set car0-eth0 master bond0')
    car[0].cmd('ip link set car0-wlan0 down')
    car[0].cmd('ip link set car0-wlan0 address 00:00:00:00:00:15')
    car[0].cmd('ip link set car0-wlan0 master bond0')
    car[0].cmd('ip link set car0-wlan1 down')
    car[0].cmd('ip link set car0-wlan1 address 00:00:00:00:00:13')
    car[0].cmd('ip link set car0-wlan1 master bond0')
    car[0].cmd('ip addr add 200.0.10.100/24 dev bond0')
    car[0].cmd('ip link set bond0 up')

    car[3].cmd('ifconfig car3-wlan0 200.0.10.150')

    client.cmd('ip route add 192.168.1.8 via 200.0.10.150')
    client.cmd('ip route add 10.0.0.1 via 200.0.10.150')

    net.vehiclesSTA[3].cmd('ip route add 200.0.10.2 via 192.168.1.7')
    net.vehiclesSTA[3].cmd('ip route add 200.0.10.100 via 10.0.0.1')
    net.vehiclesSTA[0].cmd('ip route add 200.0.10.2 via 10.0.0.4')

    car[0].cmd('ip route add 10.0.0.4 via 200.0.10.50')
    car[0].cmd('ip route add 192.168.1.7 via 200.0.10.50')
    car[0].cmd('ip route add 200.0.10.2 via 200.0.10.50')
    car[3].cmd('ip route add 200.0.10.100 via 192.168.1.8')

    """plot graph"""
    net.plotGraph(max_x=250, max_y=250)

    net.startGraph()

    # Uncomment and modify the two commands below to stream video using VLC 
    car[0].cmdPrint("vlc -vvv bunnyMob.mp4 --sout '#duplicate{dst=rtp{dst=200.0.10.2,port=5004,mux=ts},dst=display}' :sout-keep &")
    client.cmdPrint("vlc rtp://@200.0.10.2:5004 &")

    car[0].moveNodeTo('95,100,0')
    car[1].moveNodeTo('80,100,0')
    car[2].moveNodeTo('65,100,0')
    car[3].moveNodeTo('50,100,0')

    os.system('ovs-ofctl del-flows switch')
   
    time.sleep(3)

    apply_experiment(car,client,switch)

    # Uncomment the line below to generate the graph that you implemented
    graphic_1()

    graphic_2()

   
   
    os.system('rm *.apconf')
    os.system('rm *.data')

    #os.system('pkill xterm')

    print "*** Stopping network"
    net.stop()

if __name__ == '__main__':
    setLogLevel('info')
    try:
        topology()
    except:
        type = sys.exc_info()[0]
        error = sys.exc_info()[1]
        traceback = sys.exc_info()[2]
        print ("Type: %s" % type)
        print ("Error: %s" % error)
        print ("Traceback: %s" % traceback)
        if gnet != None:
            gnet.stop()
        else:
            print "No network was created..."
